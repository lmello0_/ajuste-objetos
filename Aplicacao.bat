@echo off

title Ajuste de objetos v1.0

if exist ajustelgpd\ (
    cmd /c "cd .\ajustelgpd\Scripts & activate & cd /d  ../../codigos & python aplicacao.py"
) else (
    echo Criando ambiente de execucao...
    cmd /c "cd .\codigos & python install_venv.py"
    cmd /c "color 07 & cd .\ajustelgpd\Scripts & activate & cd /d  ../../codigos & python aplicacao.py"
)