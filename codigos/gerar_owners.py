import os

class GeraOwners:

    def __init__(self, origem):
        # GERA O ARQUIVO PARA CADA OWNER
        for folders, subfolders, files in os.walk(origem):
            if folders.endswith('Destino'):
                owner = folders[folders.find('\\LGPD')+6:].replace('\\Destino', '').upper()
                arquivo = open(f'{origem}\\ARQUIVOS_PROCS\\PROCS_{owner}.sql', 'w')
                arquivo.write(f'SET SQLBLANKLINES ON\n')
                arquivo.write(f'SPOOL J:\SuporteDesenv\LGPD\OBJETOS_{owner}.LOG;\n')

                for file in files:
                    if file != 'PROCS.SQL':
                        if file.find('.csv') == -1:
                            arquivo.write(f'PROMPT J:\\SuporteDesenv\\LGPD\\{owner}\\Destino\\{file}\n')
                            arquivo.write(f'@J:\\SuporteDesenv\\LGPD\\{owner}\\Destino\\{file}\n')
                        print(f'@J:\\SuporteDesenv\\LGPD\\{owner}\\Destino\\{file}')
                arquivo.write('SPOOL OFF;')
                arquivo.close()