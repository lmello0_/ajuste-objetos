import os
from compila_objs import CompilaObjs
from conversor import Conversor
from gerar_owners import GeraOwners
from trigger_sem_ref import TriggerSemRef

# MENU QUE CHAMA AS FUNCOES
class Menu:
    def escolhas(self):
        self.clearScreen()
        entrada = 0
        origem = r'J:\SuporteDesenv\LGPD'
        # ENQUANTO O USUARIO NAO ENTRAR COM '5' A APLICACAO NAO FINALIZA
        while (entrada) != 5:
            entrada = self.printMenu()
            # CHAMA O CONVERSOR DE TABELA
            if entrada == 1:
                self.clearScreen()
                print('ATENCAO! DEIXE AS PASTAS NO SEGUINTE DIRETÓRIO "J:\SuporteDesenv\LGPD"\n')
                confirmar = input('CONTINUAR? [S/N] ').upper()
                while confirmar != 'S' and confirmar != 'N':
                    confirmar = input('CONTINUA? [S/N] ').upper()
                    
                if confirmar == 'N':
                    pass
                else:
                    Conversor(origem)

            # GERA O ARQUIVO DE COMPILACAO PARA CADA OWNER
            elif entrada == 2:
                self.clearScreen()
                GeraOwners(origem)

            # GERA O ARQUIVO COMPILA_OBJS
            elif entrada == 3:
                self.clearScreen()
                CompilaObjs(origem)
                print('COMPILA_OBJS CRIADO!')

            # MOSTRA TODAS AS TRIGGERS SEM REFERENCIA
            elif entrada == 4:
                self.clearScreen()
                TriggerSemRef(origem)

            # FINALIZA A APLICACAO
            elif entrada == 5:
                self.clearScreen()
                break

    # PRINTA O MENU, MOSTRANDO TODAS AS OPCOES DISPONIVEIS E RETORNA UM NUMERO PARA O MENU
    def printMenu(self):
        print('┌──────┬─────────────────┬──────┐')
        print('| Vida | CORRIGE OBJETOS | Link |')
        print('├──────┴─────────────────┴──────┤')
        print('| 1. CORRIGIR OBJETOS           |')
        print('| 2. GERAR ARQUIVOS OWNERS      |')
        print('| 3. GERAR COMPILA OBJS         |')
        print('| 4. TRIGGERS SEM REFERENCIA    |')
        print('| 5. SAIR                       |')
        print('└───────────────────────────────┘')
        entrada = input('Entrada.: ')

        while entrada.isnumeric() == False:
            entrada = input('Entrada.: ')

        return int(entrada)

    # LIMPA A TELA DE ACORDO COM O OS
    def clearScreen(self):
        if os.name == 'POSIX':
            os.system('CLEAR')
        elif os.name == 'nt':
            os.system('CLS')