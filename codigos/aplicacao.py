import sys, subprocess, pkg_resources

# INSTALA A DEPENDENCIA
requerimento = {'unidecode'}
instalado = {pkg.key for pkg in pkg_resources.working_set}
faltante = requerimento - instalado

if faltante:
        subprocess.check_call([sys.executable, '-m', 'pip', 'install', *faltante])

from menu import Menu

# APENAS CHAMA O MENU DA APLICACAO E ESTE CHAMA OS METODOS DA APLICACAO
menu = Menu()
menu.escolhas()

