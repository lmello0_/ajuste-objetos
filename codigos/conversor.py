import os, csv, re, unidecode, time

class Conversor:
    # IDENTIFICA MEMBER NA LINHA
    def member(self, linha):
        x = re.search(r"\b(MEMBER\s|\sPHIDBA\.MEMBER)\b", linha, re.IGNORECASE)
        if x == None:
            return False
        else:
            return True
        
    # IDENTIFICA SELECT NA LINHA
    def select(self, linha):
        x = re.search(r"\b(SELECT)\b", linha, re.IGNORECASE)
        if x == None:
            return False
        else:
            return True
    
    # SUBSTITUI A STRING PASSADA DESCONSIDERANDO LETRAS MAIUSCULAS E MINUSCULAS
    def replace(self, old, new, texto):
        src_str  = re.compile(old, re.IGNORECASE)
        return src_str.sub(new, texto)

    # REESCREVE O ARQUIVO DE ACORDO COM A LISTA PASSADA
    def reescreve(self, destino, lista, nome):
        with open(f'{destino}\\{nome}', 'w', encoding='utf-8') as arquivo:
            for linha in lista:
                arquivo.write(linha)

    # EFETUA A TROCA DAS LINHAS DO ARQUIVO
    def conversor(self, origem):
        num_arquivos = 0
        with open(r'./LOG.csv', 'w', encoding='UTF8', newline='') as log:
            escritor = csv.writer(log, delimiter=';')
            escritor.writerow(['OWNER', 'ARQUIVO','LINHA','CONTEUDO'])

            # PERCORRE POR TODAS AS PASTAS
            for folders, subfolders, files in os.walk(origem):
                if folders.upper().endswith('ORIGEM'):
                    destino = self.replace('origem', 'destino', folders)
                    owner = self.replace(r'\\destino', '', destino[destino.find('\\LGPD')+6:])

                    # PARA CADA ARQUIVO NA PASTA 'ORIGEM'
                    for file in files:
                        num_arquivos += 1
                        conteudo = []
                        
                        # ABRE O ARQUIVO E LE AS LINHAS
                        arquivo_old = open(f'{folders}\\{file}', 'r')
                        linhas_arquivo = arquivo_old.readlines()

                        # ADICIONA O _NEW NO NOME DO ARQUIVO CONVERTIDO
                        name_new = file[:file.find('.')] + '_NEW' + file[file.find('.'):]

                        num_linha_select = 0
                        num_linha = 0

                        for linha in linhas_arquivo:
                            num_linha += 1
                            # TROCA O & POR 'E' E CONVERTE O TEXTO EM ASCII PARA UNICODE
                            linha = unidecode.unidecode(linha.replace('&', 'E'))
                            
                            # CONTROLE DE LEITURA, SE O NUM_LINHA_SELECT FOR MAIOR QUE O NUM_LINHA A LINHA NAO EH LIDA
                            if num_linha < num_linha_select:
                                continue

                            # SE HOUVER SELECT NA LINHA... SE NAO HOUVER APENAS REESCREVE A LINHA
                            if self.select(linha):
                                num_linha_select = num_linha

                                # PARA A LINHA DO SELECT... ATÉ ACHAR O ';'
                                for linha_select in linhas_arquivo[num_linha-1:]:
                                    num_linha_select += 1
                                    linha_select = self.replace(r'\b(PHIDBA\.MEMBER( |\b)|MEMBER )\b', r' PHIDBA.VW_MEMBERC ', linha_select.replace('&', 'E'))
                                    linha_select = self.replace(r'\b(IIS\.RMB_CLIENTE( |\b)|RMB_CLIENTE )\b', r' IIS.VW_RMB_CLIENTEC ', linha_select.replace('&', 'E'))
                                    linha_select = self.replace(r'\b(DELIVERY\.DLV_USUARIO( |\b)|DLV_USUARIO )\b', r' DELIVERY.VW_DLV_USUARIO ', linha_select.replace('&', 'E'))

                                    # PRINTA NA TELA E REGISTRA NO LOG
                                    if linha_select.find('PHIDBA.VW_MEMBERC') != -1 or linha_select.find('IIS.VW_RMB_CLIENTEC') != -1 or linha_select.find('DELIVERY.VW_DLV_USUARIO') != -1:
                                        escritor.writerow([owner, file, num_linha_select-1, linha_select.strip()])
                                        print(f'Owner.: {owner.ljust(12)} | Arquivo: {file.ljust(35)} | Linha: {str(num_linha_select-1).rjust(5)} | {linha_select.strip()}')

                                    # APPENDA NA LISTA PARA REESCREVER O ARQUIVO
                                    conteudo.append(linha_select)

                                    if linha_select.find(';') != -1:
                                        break
                            else:
                                conteudo.append(linha)

                            arquivo_old.close()
                        
                        self.reescreve(destino, conteudo, name_new)
            print(f'\nTotal de arquivos.: {num_arquivos}')


    def __init__(self, origem):
        # CONTROLE DE TEMPO DE EXECUCAO
        inicio = time.time()
        self.conversor(origem)
        duracao = time.time() - inicio
        
        # MOSTRA A DURACAO DA EXECUCAO
        if duracao > 60:
            print(f'\nTempo de execucao.: {int(duracao/60)}m{int(round(duracao%1, 2)*60)}s')
        else:
            print(f'\nTempo de execucao.: {round(duracao, 2)}s')