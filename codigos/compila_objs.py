import os

class CompilaObjs:
    def __init__(self, origem):
        for folders, subfolders, files in os.walk(origem):
            if folders.endswith('LGPD'):
                with open(f'{folders}\\compila_objs.sql', 'w', encoding='utf-8') as arquivo:
                    arquivo.write('@J:\SuporteDesenv\LGPD\F_Decrypt.fnc\n')
                    arquivo.write('@J:\SuporteDesenv\LGPD\F_ENCRYPT.fnc\n')
                    arquivo.write('@J:\SuporteDesenv\LGPD\VW_DLV_USUARIO.VW\n')
                    arquivo.write('@J:\SuporteDesenv\LGPD\VW_MEMBERC.VW\n')
                    arquivo.write('@J:\SuporteDesenv\LGPD\VW_RMB_CLIENTEC.VW\n')
                    arquivo.write('@J:\SuporteDesenv\LGPD\grants.txt\n')
            
                    if folders.endswith('ARQUIVOS_PROCS'):
                        for file in files:
                            print(f'{folders}\\{file}')
                            arquivo.write(f'@J:\\SuporteDesenv\\LGPD\\ARQUIVOS_PROCS\\{file}\n')
