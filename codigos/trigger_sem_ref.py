import os, re, unidecode, csv

class TriggerSemRef:
    def __init__(self, destino):
        self.origem = destino
        self.converte()

    # PROCURA BEFORE OU AFTER
    def before_after(self, linha):
        x = re.search(r"\b(BEFORE|AFTER)\b", linha, re.IGNORECASE)
        if x == None:
            return False
        else:
            return True

    # PROCURA O ON
    def on(self, linha):
        x = re.search(r"\b(ON .*\.)\b", linha, re.IGNORECASE)
        if x == None:
            return False
        else:
            return True
    
    # SUBSTITUI A LINHA
    def replace(self, old, new, texto):
        src_str  = re.compile(old, re.IGNORECASE)
        return src_str.sub(new, texto)

    def converte(self):
        # ABRE O ARQUIVO PARA REGISTRO DE LOG
        with open(f'{self.origem}\\TRIGGERS_SEM_REFERENCIA.csv', 'w', encoding='UTF8', newline='') as escrita:
            escritor = csv.writer(escrita, delimiter=';')
            escritor.writerow(['OWNER', 'ARQUIVO'])

            # PARA CADA ARQUIVO
            for folders, subfolders, files in os.walk(self.origem):
                if folders.upper().endswith('DESTINO'):
                    owner = self.replace(r'\\destino', '', folders[folders.find('\\LGPD')+6:])

                    for file in files:
                        # ABRE APENAS TRIGGERS
                        if file.upper().endswith('.TRG'):
                            arquivo = open(f'{folders}\\{file}', 'r', errors='replace')
                            linhas_arquivo = arquivo.readlines()

                            num_linha = 0
                            num_linha_trg = 0
                            for linha in linhas_arquivo:
                                num_linha += 1
                                linha = unidecode.unidecode(linha)

                                # CONTROLE DE LEITURA, SE O NUM_LINHA_SELECT FOR MAIOR QUE O NUM_LINHA A LINHA NAO EH LIDA
                                if num_linha < num_linha_trg:
                                    continue
                                
                                if self.before_after(linha):
                                    cont_on = 0
                                    num_linha_trg = num_linha
                                    for linha_trg in linhas_arquivo[num_linha-1:]:
                                        num_linha_trg += 1

                                        # SE HOUVER O ON SEM O OWNER, AUMENTA O CONTADOR
                                        if self.on(linha_trg):
                                            cont_on += 1
                                        
                                    # SE NAO HOUVER REFERENCIA NO 'ON', ESCREVE NO ARQUIVO
                                    if cont_on < 1:
                                        escritor.writerow([owner, file])
                                        print(f'Owner.: {owner.ljust(11)} | Arquivo.: {file}')
                                
                                
                    


if __name__ == '__main__':
    converte = TriggerSemRef(input(''))
    converte.converte()
