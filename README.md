~~~
               _                 _                  _             ____    _         _          _                 
     /\       (_)               | |                | |           / __ \  | |       (_)        | |                
    /  \       _   _   _   ___  | |_    ___      __| |   ___    | |  | | | |__      _    ___  | |_    ___    ___ 
   / /\ \     | | | | | | / __| | __|  / _ \    / _` |  / _ \   | |  | | | '_ \    | |  / _ \ | __|  / _ \  / __|
  / ____ \    | | | |_| | \__ \ | |_  |  __/   | (_| | |  __/   | |__| | | |_) |   | | |  __/ | |_  | (_) | \__ \
 /_/    \_\   | |  \__,_| |___/  \__|  \___|    \__,_|  \___|    \____/  |_.__/    | |  \___|  \__|  \___/  |___/
             _/ |                                                                 _/ |                           
            |__/                                                                 |__/                            
~~~
---
O script funciona substituindo as tabelas:

- PHIDBA.MEMBER -> PHIDBA.VW_MEMBERC
- IIS.RMB_CLIENTE -> IIS.VW_RMB_CLIENTEC
- DELIVERY.DLV_USUARIO -> DELIVERY.VW_DLV_USUARIO
---
## Requerimentos:
<br>

| Linguagem | Versão | Link de download oficial                                                         |
|:-----------:|:--------:|:------------------------------------------------------------------:|
| Python    | 3.10.0 | https://www.python.org/ftp/python/3.10.2/python-3.10.2-amd64.exe |

<br>

Os arquivos devem estar dispostos na pasta `J:\SuporteDesenv\LGPD\` da seguinte forma 
~~~html
J:\SuporteDesenv\LGPD
                    ├---Owner
                    ├---Destino
                    │   └---Vazia
                    │
                    └---Origem
                        ├---Objeto1.prc
                        ├---Objeto2.fnc
                        ├---Objeto3.trg
                        ├---Objeto4.pks
                        └---Objeto5.pkb
~~~
<br>

---
## Como utilizar:

1. Abra um terminal
    1. <kbd>⊞ Win</kbd> + <kbd>R</kbd>
    2. 'cmd'
    3. <kbd>Enter</kbd>

![Terminal](imagens/terminal.gif)

2. Ir até a pasta em que está a aplicação
~~~
cd caminho\para\pasta\da\aplicacao
~~~

![Terminal](imagens/terminal2.gif)

3. Executar a aplicação com o seguinte comando dentro da pasta
~~~bat
.\Aplicacao.bat
~~~

![Terminal](imagens/terminal3.gif)

A aplicação preparará o ambiente para execução, baixando todas as dependências automaticamente e logo depois iniciará o programa principal. Mostrando todas as opções disponíveis.

<br>

| Opção | Descrição |
|:-----|:---------|
| 1. Corrigir Objetos | Passa por todos os arquivos e converte as tabelas |
| 2. Gerar arquivos owners | Gera o arquivo `procs_<owner>.sql`, contendo os objetos de cada owner |
| 3. Gerar compila objs | Gera o arquivo `compila_objs.sql` com os arquivos `procs` de cada owner |
| 4. Triggers sem referencia | Checa se há arquivos de triggers (`.trg`) sem referencia na tabela |
| 5. Sair | Sai do programa |
